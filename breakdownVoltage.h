#include "TGraph.h"
#include "TGraphErrors.h"
#include "TF1.h"



double getBreakdownVoltage(TGraph *graph);


double getChi2(TGraph* hist, TF1* fit, double minX, double maxX);


TF1* fit(TGraph* hist, double lowX, double highX);
