CC = g++
CFLAGS = -c -g -Wall `root-config --cflags`
LDFLATS =`root-config --glibs`


main : lgad_iv_plotting.o breakdownVoltage.o helperFunctions.o AtlasStyle.o
	$(CC) $(LDFLATS) lgad_iv_plotting.o breakdownVoltage.o helperFunctions.o AtlasStyle.o -o lgad_iv_plotting

lgad_iv_plotting.o: lgad_iv_plotting.cxx
	$(CC) $(CFLAGS) lgad_iv_plotting.cxx $<

breakdownVoltage.o: breakdownVoltage.cxx
	$(CC) $(CFLAGS) breakdownVoltage.cxx $<

helperFunctions.o: helperFunctions.cxx
	$(CC) $(CFLAGS) helperFunctions.cxx $<

AtlasStyle.o: AtlasStyle.C
	$(CC) $(CFLAGS) AtlasStyle.C $<

