#include "helperFunctions.h"
#include <cmath>
#include "TLatex.h"

// Creates a directory for saving the plots
void makeDir(std::string outDir){
  if(!opendir(outDir.c_str())){
    std::cout << "Creating directory " << outDir.c_str() << std::endl;
    const int dir_err = mkdir(outDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    if (-1 == dir_err){
      std::cout << "Error creating directory " << outDir.c_str() << std::endl;
      exit(1);
    }
  }
}



void drawHists(TCanvas* c1, std::string fileName, std::vector<TGraph*> graphs, int isLog, std::vector<std::string> histLabels, int isLogX, std::string drawStyle, double legX, double legY){
  if(graphs.size() ==0) return;
  SetAtlasStyle();
  c1->cd();
  c1->SetRightMargin(0.02);
  c1->SetBottomMargin(0.2);
  c1->SetLeftMargin(0.15);
  c1->SetTopMargin(0.08);
  c1->SetLogy(isLog);
  c1->SetLogx(isLogX);

  std::vector<TString> colors = getColors();
  std::vector<int> markerStyles = getMarkerStyles();
  int nColors = colors.size();
  int nMarkers = markerStyles.size();

  configurePlot(graphs[0], 0.9, colors[0], markerStyles[0]);
  graphs[0]->Draw("AP");
  std::vector<std::string> drawStyles;


  //std::reverse(graphs.begin(),graphs.end()); //use this only for D sweep

  for(unsigned int i=0; i < graphs.size(); i++) {
  //for(unsigned int i = graphs.size()-1; i > ; i++) {
    int colorCount = i - floor(i/nColors)*nColors;
    int markerCount = i - floor(i/nMarkers)*nMarkers;
 
    configurePlot(graphs[i], 0.9, colors[colorCount], markerStyles[markerCount]);
    graphs[i]->Draw(Form("%s", drawStyle.c_str()));
    graphs[i]->SetMarkerSize(0.5);
    drawStyles.push_back(drawStyle);
  } //Get all info for different types of subtraction

  int nColumns = 1;
  //if(graphs.size() > 6) nColumns = 2;
  //if(graphs.size() > 11) nColumns = 3;
   std::vector<TGraph*> legGraph;

   TLegend* leg = makeLegends(graphs, drawStyles, 1., legX, legY, 0.4, nColumns, 0.04);
   //TLegend* leg = makeLegends(graphs, drawStyles, 0.9, legX, legY, 0.08, nColumns, 0.08); //for stres test
   leg->Draw();
  gPad->RedrawAxis();

  TLatex *tex = new TLatex();
  //tex->SetTextFont(72);
  //tex->SetTextSize(0.03);
  //tex->DrawLatex(175,1e-11,"BNL W3045 (50 #mum)"); //for Iv vs voltage
  //tex->DrawLatex(-30,20,"BNL W3045 (50 #mum)");
  tex->DrawLatex(460,30,"BNL W3045 (50 #mum)");

  printHist(c1, fileName);
}

std::pair<double, double> getRange(double minVal, double maxVal, bool isLog, bool isZeroed){
  if(isLog) {
    std::cout << minVal << "\t" << maxVal << std::endl;
    if(minVal <=0) minVal = 1e-11;
    if(maxVal <=0) maxVal = 1e-9;
    //maxVal = maxVal + pow(10, log10(maxVal) + (log10(maxVal)-log10(minVal))/1.8);
    //maxVal = maxVal + pow(10, log10(maxVal) + (log10(maxVal)-log10(minVal))/1.8);
    maxVal = maxVal ;
    minVal = minVal / 2.;
  }

  else  {
    if(isZeroed) minVal = 0;
    double diff = maxVal - minVal;
    maxVal += diff*0.3;
    if(!isZeroed) minVal -= diff*0.10;
  }

  return std::make_pair(minVal, maxVal);
}


void getRange(std::vector<TGraph*> graphs, bool isLog, bool isZeroed, double setMinVal, double setMaxVal, double startX, double endX){
  if(setMinVal != setMaxVal) {
    if(setMinVal == 0) setMinVal = 1e-3;
    setMaxVal = setMaxVal - 1e-3;
    for(unsigned int i=0; i<graphs.size(); i++){
      graphs[i]->GetYaxis()->SetRangeUser(setMinVal, setMaxVal);
    }
    return;
  }

  double maxVal = -1000000;
  double minVal = 10000000;

  for(unsigned int fit=0; fit<graphs.size(); fit++){
    for(int i=0; i<graphs[fit]->GetN(); i++){
      if(startX != endX && (graphs[fit]->GetPointX(i) < startX || graphs[fit]->GetPointX(i) > endX)) continue;

      if(graphs[fit]->GetPointY(i) > maxVal) maxVal = graphs[fit]->GetPointY(i);
      if(graphs[fit]->GetPointY(i) < minVal) minVal = graphs[fit]->GetPointY(i);
    }
  }
  std::pair<double, double> minMax = getRange(minVal, maxVal, isLog, isZeroed);

  for(unsigned int i=0; i<graphs.size(); i++){
    graphs[i]->GetYaxis()->SetRangeUser(minMax.first, minMax.second);//for breakdown graph
    //graphs[i]->GetYaxis()->SetRangeUser(1e-13, 1e-5); //temporary change, need a real fix (for pad current)
    //graphs[i]->GetYaxis()->SetRangeUser(1e-9, 1e-5); //temporary change, need a real fix (for total current)

    //graphs[i]->GetYaxis()->SetRangeUser(1e-5, 1e2); //temporary change, need a real fix (for pad/total current
  }
}



void printHist(TCanvas *c1, std::string fileName){
  c1->Print(Form("%s_redAxe.pdf", fileName.c_str()));
  //c1->Print(Form("%s.eps", fileName.c_str()));
  //c1->Print(Form("%s.root", fileName.c_str()));
  //c1->Print(Form("%s.png", fileName.c_str()));
}

std::vector<TString> getColors(){
  std::vector<TString> colors;
  // for IV vs Volt
  colors.push_back("#3c3c3c");
  colors.push_back("#8A293E");
  colors.push_back("#D74061");
  colors.push_back("#BD4D37");
  colors.push_back("#D69340");
  colors.push_back("#BDA637");
  colors.push_back("#3A4213");
  colors.push_back("#B6D640");
  colors.push_back("#36BDBD");
  colors.push_back("#5656D7");
  colors.push_back("#2D2D70");
  colors.push_back("#49288A");

  /* // for bdv vs temp
   colors.push_back("#3c3c3c");
  //colors.push_back("#8A293E");
  //colors.push_back("#D74061");
  colors.push_back("#BD4D37");
  //colors.push_back("#D69340");
  colors.push_back("#BDA637");
  //colors.push_back("#3A4213");
  colors.push_back("#B6D640");
  //colors.push_back("#36BDBD");
  colors.push_back("#5656D7");
  //colors.push_back("#2D2D70");
  colors.push_back("#49288A");*/
  
  return colors;
}


std::vector<int> getMarkerStyles(){
  std::vector<int> markerStyles;
  markerStyles.push_back(20);
  markerStyles.push_back(21);
  markerStyles.push_back(22);
  markerStyles.push_back(23);
  markerStyles.push_back(24);
  markerStyles.push_back(25);
  markerStyles.push_back(26);
  markerStyles.push_back(27);
  markerStyles.push_back(28);
  return markerStyles;
}

void configurePlot(TGraph* hist, double padHeight, TString color, int markerStyle){
  hist->GetYaxis()->SetLabelSize(0.05 );
  hist->GetYaxis()->SetTitleSize(0.05);
  hist->GetYaxis()->SetTitleOffset(1.1);
  hist->GetYaxis()->SetTickLength(0.03);
  hist->SetMarkerColor(TColor::GetColor(color));
  hist->SetLineColor(TColor::GetColor(color));
  hist->SetMarkerStyle(markerStyle);

  hist->GetXaxis()->SetLabelSize(0.04 / padHeight);
  hist->GetXaxis()->SetTitleSize(0.04 / padHeight);
  hist->GetXaxis()->SetTitleOffset(0.95);
  hist->GetXaxis()->SetTickLength(0.03);
  hist->GetXaxis()->SetTickLength(0.02 / padHeight);
}




TLegend* makeLegends(std::vector<TGraph*> hists, std::vector<std::string> drawStyle, double padHeight, double xPos, double yPos, double dX, int nColumns, double ySpacing){
  double spacing = ySpacing / padHeight;

  double dY = hists.size() * spacing / padHeight;
  if(nColumns > 1){
    dY = (hists.size()+1)/nColumns * ySpacing / padHeight;
  }

  if(dX>1 || dX==0)
    dX = 0.97 - xPos;

  TLegend* leg = new TLegend(xPos, yPos-dY, xPos+dX, yPos, NULL,"brNDC");
  leg->SetBorderSize(0);
  leg->SetLineColor(1);
  leg->SetNColumns(nColumns);
  leg->SetLineStyle(1);
  leg->SetLineWidth(1);
  leg->SetFillColor(0);
  leg->SetFillStyle(0);
  leg->SetTextFont(42);
  leg->SetTextSize(0.03);


  for(unsigned int fit=0; fit<hists.size(); fit++){
    //if(1==0) leg->AddEntry(hists[fit], TString(hists[fit]->GetTitle()), "l");
    if(drawStyle[fit].find("hist") != std::string::npos || drawStyle[fit].find("HIST") != std::string::npos) leg->AddEntry(hists[fit], TString(hists[fit]->GetTitle()), "l");
    else{
        leg->AddEntry(hists[fit], hists[fit]->GetTitle(), "p");
    }
  }
  return leg;
}



