#include <string>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <iostream>
#include "TGraphErrors.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "TH1.h"
#include "TLegend.h"
#include "TColor.h"
#include "AtlasStyle.h"

void makeDir(std::string outDir);

void drawHists(TCanvas* c1, std::string fileName, std::vector<TGraph*> graphs, int isLog, std::vector<std::string> histLabels, int isLogX, std::string drawStyle = "HIST", double legX = 0.2, double legY = 0.9);

std::pair<double, double> getRange(double minVal, double maxVal, bool isLog, bool isZeroed);

void getRange(std::vector<TGraph*> graphs, bool isLog, bool isZeroed, double setMinVal=0, double setMaxVal=0, double startX=0, double endX=0);

void configurePlot(TGraph* hist, double padHeight, TString color, int markerStyle);

void printHist(TCanvas *c1, std::string fileName);

TLegend* makeLegends(std::vector<TGraph*> hists, std::vector<std::string> drawStyle, double padHeight, double xPos, double yPos, double dX, int nColumns, double ySpacing);

std::vector<TString> getColors();

std::vector<int> getMarkerStyles();







