#include "breakdownVoltage.h"

double getChi2(TGraph* hist, TF1* fit, double minX, double maxX){
  double chi2 = 0;
  int n = 0;
  for(int i=0; i<hist->GetN(); i++){
    double cX = (hist->GetX())[i];
    double cY = (hist->GetY())[i];
    double funcVal = fit->Eval(cX);
    if(cX < minX || cX > maxX) continue;
    //chi2 += ( cY - funcVal) * (cY - funcVal)/(cY*cY) ;
    //chi2 += ( cY - funcVal) * (cY - funcVal)/(cY*cY) ;
    // Trying to ignore outliers -- should always be smoothly increasing
    if( hist->GetY()[i] < hist->GetY()[i+1] && hist->GetY()[i] < hist->GetY()[i-1]) continue;
    if( hist->GetY()[i] > hist->GetY()[i+1] && hist->GetY()[i] > hist->GetY()[i-1]) continue;
    if( hist->GetY()[i] < hist->GetY()[i-1]) continue;
    
    chi2 += std::abs( cY - funcVal)/(cY) ;
    //chi2 += std::abs( cY - funcVal) ;
    //chi2 = std::abs( cY - funcVal)/cY;
    //std::cout << cX << "\t" << cY << "\t" << funcVal << "\t" << chi2 << std::endl;
    n++;
  }
  //return sqrt(chi2) / (n+4);
  return chi2 / (n);
  
  //return chi2 ;


}



TF1* fit(TGraph* hist, double lowX, double highX){
  TF1* fitFunc = new TF1(Form("fitLin_%.0f_%.0f", lowX, highX), "1e-11*([0]+[1]*x)", lowX, highX);
  hist->Fit(fitFunc, "SQ", "", lowX, highX);
  // Fit twice to get a better fit
  hist->Fit(fitFunc, "SQ", "", lowX, highX);

  return fitFunc;

}


double getBreakdownVoltage(TGraph *graph){

  int Npoints = graph->GetN();
  TF1* linear = new TF1("linear", "1e-11*([1]*x)", 0, 250);
  TF1* exp = new TF1("myexp", "1e-11*([0]*exp([1]*x)-[0])", 0, 250);

  TF1* fitResult = new TF1("fitLinExp", "1e-11*([0] - [2] + [1]*x + [2]*exp([3]*x))", 26, 250);
  //TF1* fitResult = new TF1("fitLinExp", "1e-11*([0] - [2] + [1]*x + [2]*exp([3]*x)*0)", 26, 250); 


  TF1* fitTest = fit(graph, 26, 50);

  fitResult->SetParameter(0, fitTest->GetParameter(0) );
  fitResult->SetParLimits(0, fitTest->GetParameter(0) , fitTest->GetParameter(0) );
  fitResult->SetParameter(1, fitTest->GetParameter(1));
  fitResult->SetParLimits(1, fitTest->GetParameter(1) , fitTest->GetParameter(1) );

  // Original!
  fitResult->SetParLimits(2, 1e-6, 5e-1);
  fitResult->SetParameter(2, 5e-2);
  fitResult->SetParLimits(3, 1e-6, 5e-1);
  fitResult->SetParameter(3, 5e-2);

/*
  fitResult->SetParLimits(2, 1e-5, 1e-2);
  //fitResult->SetParLimits(2, 1e-6, 1e-1);
  //fitResult->SetParLimits(2, 1e-6, 5e-1);
  fitResult->SetParameter(2, 5e-3);

  fitResult->SetParLimits(3, 1e-2,8e-1);
  fitResult->SetParameter(3, 1e-1);
*/

   double bdvSum = 0;
  int finalJ = 100;
  double par3 = 0;
  double par4 = 0;

  for(unsigned int j=55; j<250; j+=5){
    graph->Fit(fitResult, "Q1", "", 26, j);
  //std::cout << fitResult->GetParameter(0)  << "\t" << fitResult->GetParameter(1) << "\t" << fitResult->GetParameter(2) << "\t" << fitResult->GetParameter(3) << std::endl;
    graph->Fit(fitResult, "Q1", "", 26, j);
  //std::cout << fitResult->GetParameter(0)  << "\t" << fitResult->GetParameter(1) << "\t" << fitResult->GetParameter(2) << "\t" << fitResult->GetParameter(3) << std::endl;
  //  graph->Fit(fitResult, "Q1", "", 26, j);
  //std::cout << fitResult->GetParameter(0)  << "\t" << fitResult->GetParameter(1) << "\t" << fitResult->GetParameter(2) << "\t" << fitResult->GetParameter(3) << std::endl;
  //std::cout << std::endl;
    //graph->Fit(fitResult, "Q1", "", 26, j);
    // This is not at all a chi2, but just trying to get some quantity that says if the fit is degrading
    //double fitQNew = getChi2(graph, fitResult,j-10, j);
    double fitQNew = getChi2(graph, fitResult,26, j);
    //std::cout << j << "\t" << fitQNew << std::endl;
    if(fitQNew <0.05){
       finalJ = j;
       par3 = fitResult->GetParameter(2);
       par4 = fitResult->GetParameter(3);
    }
    else{
      continue;
      //break;
    }

    linear->SetParameter(0,0);
    linear->SetParameter(1, fitResult->GetParameter(1));
    exp->SetParameter(0, fitResult->GetParameter(2));
    exp->SetParameter(1, fitResult->GetParameter(3));
    for(unsigned int i=0; i<graph->GetPointX(Npoints-1); i++){
      if(linear->Eval(graph->GetPointX(i)) < exp->Eval(graph->GetPointX(i))){
         if(graph->GetPointX(i) < 30) continue;
         if(graph->GetPointX(i) > j) continue;
         bdvSum = graph->GetPointX(i);
         break;
      }
    }
  }

  std::cout << finalJ << "\t" << bdvSum <<std::endl;
  TF1* fitResultFinal = new TF1(Form("fitLinExpFinal_%s",graph->GetName()), "1e-11*([0] - [2] + [1]*x + [2]*exp([3]*x))", 26, 250); 
  fitResultFinal->SetParameter(0, fitTest->GetParameter(0) );
  fitResultFinal->SetParLimits(0, fitTest->GetParameter(0) , fitTest->GetParameter(0) );
  fitResultFinal->SetParameter(1, fitTest->GetParameter(1));
  fitResultFinal->SetParLimits(1, fitTest->GetParameter(1) , fitTest->GetParameter(1) );
  fitResultFinal->SetParLimits(2, par3, par3);
  fitResultFinal->SetParameter(2, par3);
  fitResultFinal->SetParLimits(3, par4, par4);
  fitResultFinal->SetParameter(3, par4);
  fitResultFinal->SetLineColor(kBlue);
  fitResultFinal->SetLineWidth(2);

graph->Fit(fitResultFinal, "Q1", "", 26, finalJ);
  std::cout << fitResult->GetParameter(0)  << "\t" << fitResult->GetParameter(1) << "\t" << fitResult->GetParameter(2) << "\t" << fitResult->GetParameter(3) << std::endl;


  /* double bdvSum =-1;
  int finalJ = 100;
  double par3 = 0;
  double par4 = 0;
  double rangeLow = 40;
  double rangeHigh = 250;
  TF1* linear1 = new TF1("linear1", "[0]+[1]*x", rangeLow, 70);
  TF1* expfunc = new TF1("expfunc", "[0]+[1]*x+[2]*TMath::Exp([3]*x)", rangeLow,140);
  unsigned int j;

  double factor=5.;
  std::cout<<"linear fit "<<std::endl;
  graph->Fit(linear1, "", "", rangeLow, 70);
  for (int i=0; i< linear1->GetNpar() ;i++)
    {
      linear1->FixParameter(i,linear1->GetParameter(i));
    }
  std::cout<<"Full fit "<<std::endl;
  graph->Fit(expfunc,"","",rangeLow,140);
  for (int i=0; i< expfunc->GetNpar() ;i++)
    {
      expfunc->FixParameter(i,expfunc->GetParameter(i));
    }
  
  
  std::cout << " debug " << std::endl;
  std::vector<std::pair<double,std::vector<double>>> vals;
  for(j=rangeLow; j<rangeHigh; j+=5){
    TF1* linearVar = new TF1("linear", "[0]+[1]*x", rangeLow, j+2);
    TF1* expVar= new TF1("expVar","[0]+[1]*x+[2]*TMath::Exp([3]*x)", rangeLow,j+2);
    for (int i=0; i< linear1->GetNpar() ;i++)
    {
      linearVar->FixParameter(i,linear1->GetParameter(i));
      
    }
    for (int i=0; i< expfunc->GetNpar() ;i++)
      {
      expVar->FixParameter(i,expfunc->GetParameter(i));
    }
    
    
    graph->Fit(linearVar,"","",rangeLow,j+2); 
    graph->Fit(expVar,"","",rangeLow,j+2);
    std::vector <double> chi2={linearVar->GetChisquare()/linearVar->GetNDF(),expVar->GetChisquare()/expVar->GetNDF()};
    vals.push_back(std::make_pair(j,chi2));
    delete linearVar;
    delete expVar;
    if( j>rangeLow && vals.back().second[1] / vals.at(vals.size()-2).second[0] > factor ) { bdvSum = vals.back().first; break;}
  }
 

   TF1* linearFinal = new TF1("linearFinal", "[0]+[1]*x", rangeLow,bdvSum+2);
   for (int i=0; i< linear1->GetNpar() ;i++)
     {
       linearFinal->SetParameter(i,linear1->GetParameter(i));
       
     }
   linearFinal->SetLineColor(kBlue);
   
   TF1* expFinal= new TF1("expFinal","[0]+[1]*x+[2]*TMath::Exp([3]*x)", rangeLow,j+2);
   
   for (int i=0; i< expfunc->GetNpar() ;i++)
     {
       expFinal->SetParameter(i,expfunc->GetParameter(i));
       
     }
   
   //graph->Fit(linearFinal,"","",rangeLow, bdvSum-5);
   graph->Fit(expFinal,"","",rangeLow, 150);
   
   std::cout << "bdvSum " << bdvSum << std::endl;*/
   
   //graph->Fit(fitResultFinal, "Q1", "", 26, finalJ);
  //std::cout << fitResult->GetParameter(0)  << "\t" << fitResult->GetParameter(1) << "\t" << fitResult->GetParameter(2) << "\t" << fitResult->GetParameter(3) << std::endl;

  return bdvSum;
}





