#include <getopt.h>
#include<stdlib.h> // atof
#include <vector>
#include <string.h>
#include "fstream"
#include "iostream"

#include "TStyle.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TF1.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TMultiGraph.h"

#include "lgad_iv_plotting.h"
#include "helperFunctions.h"
#include "breakdownVoltage.h"

using namespace std;



int main(int argc, char *argv[]) {

  std::string fileList = "fileList.txt"; // Name of file with list of all input files
  std::string outdir = "IVPlots"; // Directory where plots are stored
  std::string outfile = "test"; // Directory where plots are stored
  
  std::string rowStartName = "BEGIN"; //A unique string that identifies the row before the data starts
  std::string rowEndName = "END"; //A unique string that identifies the row after the data ends
  //std::string rowEndName = ""; //A unique string that identifies the row after the data ends
  int voltageColumn = 0; //The data column where the voltage is stored (index starting at 0)
  int padCurrentColumn = 2; //The data column where the current is stored (index starting at 0)
  int totalCurrentColumn = 1; //The data column where the current is stored (index starting at 0)
  std::string separator = "\t"; // The string between data columns to separate them
  std::string temperatureString = ":temperature[C]";
  //std::string temperatureString = "";
  int minVoltage = 0;
  int maxVoltage = 220;
   

  std::vector<std::string> allFileLists;
  std::vector<std::string> names;
  std::vector<std::string> sweepName;

  std::vector<std::string> deltaT;
 
  sweepName.push_back("measurement 1");
  sweepName.push_back("measurement 2");
  sweepName.push_back("measurement 3");
  sweepName.push_back("measurement 4");

  deltaT.push_back("#Deltat(s) 390");
  deltaT.push_back("#Deltat(s) 497");
  deltaT.push_back("#Deltat(s) 395");
  deltaT.push_back("#Deltat(s) 357");
  deltaT.push_back("#Deltat(s) 792");
  deltaT.push_back("#Deltat(s) 307");
  deltaT.push_back("#Deltat(s) 326");
  deltaT.push_back("#Deltat(s) 334");

 

  static struct option long_options[] =
  {
    {"fileList", 1, NULL, 'a'},
    {"outdir", 1, NULL, 'b'},
    {"outfile", 1, NULL, 'c'},
    {"rowStartName", 1, NULL, 'd'},
    {"separator", 1, NULL, 'e'},
    {"temperatureString", 1, NULL, 'f'},
    {"voltageColumn", 1, NULL, 'g'},
    {"totalCurrentColumn", 1, NULL, 'h'},
    {"padCurrentColumn", 1, NULL, 'i'},
    {"name", 1, NULL, 'j'},
    {"rowEndName", 1, NULL, 'k'},
    {"minVoltage", 1, NULL, 'l'},
    {"maxVoltage", 1, NULL, 'm'},
    {NULL, 0, NULL, 0}
  };

  int opt;
  while ( (opt = getopt_long(argc, argv,"abcdefghijkopqrstuvwxyz", long_options, NULL)) != -1 ) {  // for each option...
    switch ( opt )
      {
      //case 'a': fileList = optarg; break;
      case 'a': allFileLists.push_back(optarg); break;
      case 'b': outdir = optarg; break;
      case 'c': outfile = optarg; break;
      case 'd': rowStartName = optarg; break;
      case 'e': separator = optarg; break;
      case 'f': temperatureString = optarg; break;
      case 'g': voltageColumn = atoi(optarg); break;
      case 'h': totalCurrentColumn = atoi(optarg); break;
      case 'i': padCurrentColumn = atoi(optarg); break;
      case 'j': names.push_back(optarg); break;
      case 'k': rowEndName = optarg; break;
      case 'l': minVoltage = atoi(optarg); break;
      case 'm': maxVoltage = atoi(optarg); break;
      case 0: break;
      }
  }

  TCanvas *c1 = new TCanvas("c1","Graph Draw Options", 200,10,600,400);
   c1->SetTicks();
  // Make sure the directory exists for the output plots
  makeDir(outdir);
float ddeltaT[4]= {390., 497., 395., 357.};
 
  std::vector<TGraph*> allGraphs;

  for(unsigned int fileSet=0; fileSet<allFileLists.size(); fileSet++){
    fileList = allFileLists[fileSet];

    // Read the list of files that should be processed
    std::ifstream fileListInput(fileList.c_str());
    vector<string> fileNames;
    string fileName;
    while( fileListInput >> fileName){
      fileNames.push_back(fileName);
    }

    TGraph* grBDV = new TGraph();
    //grBDV->SetTitle(names[fileSet].c_str());
    //grBDV->SetTitle(sweepName[fileSet].c_str());
    grBDV->GetXaxis()->SetTitle("Temperature [C]");
    //grBDV->GetXaxis()->SetTitle("#Deltat [s]");
    grBDV->GetYaxis()->SetTitle("Breakdown voltage [V]");

    allGraphs.push_back(grBDV);

    std::vector<TGraph*> gr_A;
    gr_A.resize(fileNames.size());

    for(unsigned int iplt =0; iplt <fileNames.size(); iplt++) {
      int linb_match_begin;
      int linb_match_end;
      int linb_match_temp;
  
      
      float tempVal = getLineNumbers(fileNames[iplt], temperatureString, rowStartName, rowEndName, &linb_match_temp, &linb_match_begin, &linb_match_end);
      float deltatTemp= ddeltaT[iplt];
      
      cout << tempVal << endl;
      if(strcmp(temperatureString.c_str(), "")==0){
        tempVal = atof(fileNames[iplt].substr(15, 2).c_str());
        std::cout << " file name " << fileNames[iplt].substr(15, 2) <<  " " << fileNames.size() << std::endl;
	    }
    


      int  Npoints = linb_match_end - linb_match_begin - 3;
  
      float voltage[Npoints];
      float total_current[Npoints];
      float pad_current[Npoints];
      float padOtotal_current[Npoints];
      std::cout << linb_match_begin << "\t" << linb_match_end  << std::endl;

      readVoltAndCurrent(fileNames[iplt], separator, linb_match_begin, Npoints, voltage, pad_current, total_current, voltageColumn, padCurrentColumn, totalCurrentColumn);

      //cout << " test " << pad_current[0] <<  endl;

      for(int i =0; i <Npoints; i++)
	{
	  padOtotal_current[i] =  pad_current[i]/total_current[i];
	  //padOtotal_current[i] =  total_current[i] - pad_current[i];
	 //cout << " total "<< total_current[i] << " pad "<< pad_current [i] << endl; 
	}
      cout << " test " << padOtotal_current[0] <<  endl;
      //gr_A[iplt]  = new TGraph(Npoints,voltage,total_current);
      //gr_A[iplt]  = new TGraph(Npoints,voltage,padOtotal_current);
      //gr_A[iplt]  = new TGraph(Npoints,voltage,padOtotal_current);
      gr_A[iplt]  = new TGraph(Npoints,voltage,pad_current);
      gr_A[iplt]->SetTitle(Form("T = %.0f #circC",tempVal));
      //gr_A[iplt]->SetTitle(deltaT[iplt].c_str()); //use this only for temp plots
      gr_A[iplt]->GetXaxis()->SetTitle("Voltage [V]");
      gr_A[iplt]->GetYaxis()->SetTitle("Pad current [A]");
      //gr_A[iplt]->GetYaxis()->SetTitle("Total current [A]");
      //gr_A[iplt]->GetYaxis()->SetTitle("pad/total current [A]");
      //gr_A[iplt]->GetYaxis()->SetTitle("total-Pad current [A]");
      gr_A[iplt]->GetXaxis()->SetRangeUser(minVoltage, maxVoltage);

      double breakdownVoltage = getBreakdownVoltage(gr_A[iplt]);
      grBDV->SetPoint( grBDV->GetN(), tempVal, breakdownVoltage);
      //grBDV->SetPoint( grBDV->GetN(), deltatTemp, breakdownVoltage);
      //if(breakdownVoltage ==0) grBDV->RemovePoint(iplt);
      cout << " breakdownVoltage " << breakdownVoltage << endl;
    }

    std::vector<std::string> histLabels;
    //grBDV->Print("all");

    getRange(gr_A, true, false, 0, 0, 30, maxVoltage);
    gr_A[0]->GetXaxis()->SetLimits(minVoltage, maxVoltage);
    
    
    drawHists(c1, Form("%s/IV_%s", outdir.c_str(), names[fileSet].c_str()), gr_A, true, histLabels, false, "P", 0.76, 0.9);
   

    getRange(gr_A, false, true, 0, 0, 30, 60);
    drawHists(c1, Form("%s/Linear_IV_%s", outdir.c_str(), names[fileSet].c_str()), gr_A, true, histLabels, false, "P", 0.3, 0.9);
  }

  getRange(allGraphs, false, true);
  std::vector<std::string> histLabels;
  drawHists(c1, Form("%s/BDVvsT", outdir.c_str()), allGraphs, false, histLabels, false, "P", 0.6, 0.6);
}




float getLineNumbers(std::string fileName, std::string temperatureString, std::string rowStartName, std::string rowEndName, int* tempLine, int* beginLine, int* endLine) {
  int lineCounter = 0;
  ifstream fin(fileName.c_str());
  std::string line;
  float tempVal = 0;
  if (fin.is_open()) {
    while ( getline (fin,line) ) {
      lineCounter++;

      if (line.find(temperatureString.c_str()) != string::npos) {
        *tempLine = lineCounter;
        getline(fin, line);
        lineCounter++;
        tempVal = atof(line.c_str());
      }

     if (line.find(rowStartName.c_str()) != string::npos) {
       *beginLine = lineCounter;
     }

     if (line.find(rowEndName) != string::npos) {
       *endLine = lineCounter;
     }
   }
   if(strcmp(rowEndName.c_str(), "")==0){
     *endLine = lineCounter-1;
   }
 }

  return tempVal;
}


void readVoltAndCurrent(std::string fileName, std::string separator, int lineStart, int Npoints, float* voltage, float* pad_current, float* total_current, int voltageColumn, int padCurrentColumn, int totalCurrentColumn) {
  std::string line;

  ifstream fin(fileName.c_str());
  fin.seekg(0, std::ios::beg);

  // Ignore all of the header information
  for (Int_t i=0; i<(lineStart) + 1; i++) {
     getline(fin, line);
  }

  // Get the data!
  for (Int_t i=0; i<Npoints; i++)  {
    getline (fin,line);
    std::vector<std::string> values;
    // Remove trailing character (may need to be more configurable, but our current inputs have this)
    line = line.substr(0, line.size()-1);

    size_t cpos = 0;
    while(line.find(separator.c_str(), cpos)!=std::string::npos){
      values.push_back( line.substr(cpos, line.find(separator.c_str(), cpos)-cpos));
      cpos = line.find(separator.c_str(), cpos) + 1;
    }
    values.push_back(line.substr(cpos, line.size()-cpos));

    voltage[i] = abs(atof(values[voltageColumn].c_str()));
    pad_current[i] = abs(atof(values[padCurrentColumn].c_str()));
    total_current[i] = abs(atof(values[totalCurrentColumn].c_str()));
    //cout << "test " << voltage[i] << endl;
  }
}






