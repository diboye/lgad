using namespace std;
void linNumFunct (ifstream & infile, string line, int lineCounter, vector<string>& temp, int* tempLine, int* beginLine, int* endLine)
{

 if (infile.is_open())
	   {
	     while ( getline (infile,line) )
	       {
		 lineCounter++;
		 temp.push_back(line);
		 if (line.find(":temperature[C]") != string::npos)
		   
		   {
		     
		     *tempLine = lineCounter;
		   }
		 
		 if (line.find("BEGIN") != string::npos)
		   
		   {
		     *beginLine = lineCounter;
		     //cout << "Match at line " << line_counter << endl;
		   }
		 
		 
		 if (line.find("END") != string::npos)
		   
		   {
		     *endLine = lineCounter;
		     //cout << "Match at line " << line_counter << endl;
		   }
	       }
	     //fin.close();
	   }
	 
	 infile.clear(); 
	 infile.seekg(0, std::ios::beg);
}

void readHeaderFile(ifstream & infile, string line)
{
  if (infile.is_open())
    {
      while ( getline (infile,line) )
	{
	  if (line.find("BEGIN") != string::npos) break;
	  
	}
      //fin.close();
    }
}

void readVoltAndCurrent(ifstream & infile, int Npoints, TString tmps)
{
  for (Int_t i=0; i<Npoints; i++)
	   {
	     infile>>tmps; voltage[i]=abs(tmps.IsFloat()?tmps.Atof():0);
	     infile>>tmps; total_current[i]=abs(tmps.IsFloat()?tmps.Atof():0);
	     infile>>tmps; pad_current[i]=abs(tmps.IsFloat()?tmps.Atof():0);
	   }
}
